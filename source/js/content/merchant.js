console.log('[ M3T ] merchant script is injected')

// here we may get data from background

const M3T = {}

M3T.createSlider = function() {
	chrome.storage.sync.get('merchant', ({
		merchant
	}) => {
		console.log('[ M3T ] chrome.storage.sync.get(merchant:', merchant)

		let slider = document.createElement('div')
		slider.setAttribute('id', 'm3t-slider')
		slider.style.position = 'absolute'
		slider.style.top = '30px'
		slider.style.right = '40px'
		slider.style.zIndex = '99999'
		slider.style.width = '200px'
		slider.style.height = '100px'
		slider.style.margin = 'auto'
		slider.style.padding = '40px'
		slider.style.border = '2px solid #ccc'
    slider.style.background = 'aqua'
    slider.style.borderRadius = '20px'
    slider.style.display = 'flex'
    slider.style.alignItems = 'center'

		slider.innerText = merchant

		document.querySelector('html').appendChild(slider)

	})
}

M3T.createSlider()