console.log('init content script is injected')

chrome.storage.sync.get("color", ({
  color
}) => {
  console.log('[ M3T ] chrome.storage.sync.get(color:', color)
})

chrome.runtime.sendMessage({load: 'complete'}, function(response) {
  console.log('[ M3T ] response:', response)
})
