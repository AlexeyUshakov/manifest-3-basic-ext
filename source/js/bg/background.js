let color = '#3aa757'

const M3T = {}

M3T.merchants = null
M3T.match = function (url) {
  console.log('[ M3T ] match(url:', url)
  if (M3T.merchants && M3T.merchants.length) {
    let merchant = M3T.merchants.find(m => {
      let rx = new RegExp(m.matchPattern)
      let match = rx.exec(url)
      console.log('[ M3T ] match:', match)

      if (match && match.length) {
        return true
      }
      return false
    })
    console.log('[ M3T ] merchant:', merchant)

    if (merchant) {
      chrome.storage.sync.set({
        merchant: merchant.storeName
      })

      return 'merchant'
    }
  }


  return 'default'
}

chrome.runtime.onInstalled.addListener(() => {
  // console.log('[ M3T ] chrome.runtime.id:', chrome.runtime.id)
  chrome.storage.sync.set({
    color
  })
  console.log('[ M3T ] Default background color set to %cgreen', `color: ${color}`)

  // fetch merchants
  fetch('https://softomate.net/ext/manifest3template/merchants.json')
    .then(response => response.json())
    .then(json => {
      console.log('[ M3T ] fetch merchants:', json)
      M3T.merchants = json.merchants
    })
})

chrome.action.setBadgeBackgroundColor({
    color: [0, 255, 0, 0]
  }, // Green
  () => {
    /* ... */
  },
)

chrome.action.setBadgeBackgroundColor({
    color: '#00FF00'
  }, // Also green
  () => {
    /* ... */
  },
)

chrome.action.setBadgeBackgroundColor({
    color: 'green'
  }, // Also, also green
  () => {
    /* ... */
  },
)

chrome.action.onClicked.addListener((tab) => {
  console.log('[ M3T ] chrome.action.onClicked.addListener( tab:', tab)

  chrome.scripting.executeScript({
    target: {
      tabId: tab.id
    },
    files: ['js/content.js']
  })
})

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  console.log('[ M3T ] tabId:', tabId)
  console.log('[ M3T ] changeInfo:', changeInfo)
  console.log('[ M3T ] tab:', tab)

  if (changeInfo && changeInfo.status === 'complete') {
    chrome.scripting.executeScript({
      target: {
        tabId
      },
      files: ['js/content/init.js']
    })
  }
})

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    console.log('[ M3T ] sender.tab:', sender.tab)
    console.log(sender.tab ?
      '[ M3T ] from a content script:' + sender.tab.url :
      '[ M3T ] from the extension')
    if (sender.tab && sender.tab.url && request.load === 'complete') {
      // check merchant
      // set active tab state to storage
      let state = M3T.match(sender.tab.url)

      sendResponse({
        state
      })

      if (state === 'default') {
        return
      }

      chrome.scripting.executeScript({
        target: {
          tabId: sender.tab.id
        },
        files: ['js/content/merchant.js']
      })
    }
  }
)