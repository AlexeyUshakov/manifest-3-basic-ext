let changeColor = document.getElementById("changeColor")

chrome.storage.sync.get("color", ({
    color
}) => {
    changeColor.style.backgroundColor = color
})

changeColor.addEventListener("click", async () => {
    let [tab] = await chrome.tabs.query({
        active: true,
        currentWindow: true
    })

    chrome.scripting.executeScript({
        target: {
            tabId: tab.id
        },
        function: setPageBackgroundColor,
    })
})

function setPageBackgroundColor() {
    chrome.storage.sync.get("color", ({
        color
    }) => {
        console.log('chrome.storage.sync.get(color', color)
        document.body.style.backgroundColor = color
    })
}

chrome.action.setBadgeBackgroundColor({
        color: [0, 255, 0, 0]
    }, // Green
    () => {
        /* ... */ },
)

chrome.action.setBadgeBackgroundColor({
        color: '#00FF00'
    }, // Also green
    () => {
        /* ... */ },
)

chrome.action.setBadgeBackgroundColor({
        color: 'green'
    }, // Also, also green
    () => {
        /* ... */ },
)